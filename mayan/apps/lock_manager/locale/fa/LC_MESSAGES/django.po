# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Mehdi Amani <MehdiAmani@toorintan.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:44-0400\n"
"PO-Revision-Date: 2018-09-12 07:47+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Persian (http://www.transifex.com/rosarior/mayan-edms/language/fa/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fa\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:10 settings.py:10
msgid "Lock manager"
msgstr "مدیر قفل"

#: models.py:14
msgid "Creation datetime"
msgstr "زمان ایجاد"

#: models.py:17
msgid "Timeout"
msgstr "اتمام وقت"

#: models.py:20
msgid "Name"
msgstr "نام"

#: models.py:26
msgid "Lock"
msgstr "قفل"

#: models.py:27
msgid "Locks"
msgstr "قفلها"

#: settings.py:15
msgid "Path to the class to use when to request and release resource locks."
msgstr ""

#: settings.py:23
msgid ""
"Default amount of time in seconds after which a resource lock will be "
"automatically released."
msgstr ""
