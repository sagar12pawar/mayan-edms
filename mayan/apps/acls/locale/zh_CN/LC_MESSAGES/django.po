# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Ford Guo <agile.guo@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:42-0400\n"
"PO-Revision-Date: 2018-10-11 03:19+0000\n"
"Last-Translator: Ford Guo <agile.guo@gmail.com>\n"
"Language-Team: Chinese (China) (http://www.transifex.com/rosarior/mayan-edms/language/zh_CN/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: apps.py:15 links.py:37
msgid "ACLs"
msgstr "访问控制列表"

#: apps.py:23 models.py:47
msgid "Role"
msgstr "角色"

#: apps.py:26 links.py:46 models.py:43 workflow_actions.py:48
msgid "Permissions"
msgstr "权限"

#: links.py:32
msgid "Delete"
msgstr "删除"

#: links.py:41
msgid "New ACL"
msgstr "新建访问控制表"

#: managers.py:57 managers.py:96
#, python-format
msgid "Insufficient access for: %s"
msgstr "访问: %s权限不足"

#: models.py:55
msgid "Access entry"
msgstr "访问入口"

#: models.py:56
msgid "Access entries"
msgstr "多个访问入口"

#: models.py:60
#, python-format
msgid "Permissions \"%(permissions)s\" to role \"%(role)s\" for \"%(object)s\""
msgstr "把 \"%(object)s\"的权限\"%(permissions)s\"给角色\"%(role)s\""

#: models.py:77
msgid "None"
msgstr "无"

#: permissions.py:7
msgid "Access control lists"
msgstr "访问控制列表"

#: permissions.py:10
msgid "Edit ACLs"
msgstr "编辑访问控制列表"

#: permissions.py:13
msgid "View ACLs"
msgstr "查看访问控制列表"

#: serializers.py:24 serializers.py:132
msgid ""
"API URL pointing to the list of permissions for this access control list."
msgstr "指向这个访问控制列表的权限列表的API URL"

#: serializers.py:57
msgid ""
"API URL pointing to a permission in relation to the access control list to "
"which it is attached. This URL is different than the canonical workflow URL."
msgstr ""

#: serializers.py:87
msgid "Primary key of the new permission to grant to the access control list."
msgstr ""

#: serializers.py:111 serializers.py:187
#, python-format
msgid "No such permission: %s"
msgstr "无此权限: %s"

#: serializers.py:126
msgid ""
"Comma separated list of permission primary keys to grant to this access "
"control list."
msgstr ""

#: serializers.py:138
msgid "Primary keys of the role to which this access control list binds to."
msgstr ""

#: views.py:77
#, python-format
msgid "New access control lists for: %s"
msgstr "为 %s新建访问控制列表"

#: views.py:104
#, python-format
msgid "Delete ACL: %s"
msgstr "删除访问控制列表: %s"

#: views.py:148
msgid "There are no ACLs for this object"
msgstr "无此对象的访问控制列表"

#: views.py:151
msgid ""
"ACL stands for Access Control List and is a precise method  to control user "
"access to objects in the system."
msgstr ""

#: views.py:155
#, python-format
msgid "Access control lists for: %s"
msgstr "%s的访问控制列表"

#: views.py:167
msgid "Available permissions"
msgstr "有效的权限"

#: views.py:168
msgid "Granted permissions"
msgstr "被授权的权限"

#: views.py:230
#, python-format
msgid "Role \"%(role)s\" permission's for \"%(object)s\""
msgstr ""

#: views.py:250
msgid "Disabled permissions are inherited from a parent object."
msgstr ""

#: workflow_actions.py:25
msgid "Object type"
msgstr "对象类型"

#: workflow_actions.py:28
msgid "Type of the object for which the access will be modified."
msgstr ""

#: workflow_actions.py:34
msgid "Object ID"
msgstr "对象ID"

#: workflow_actions.py:37
msgid ""
"Numeric identifier of the object for which the access will be modified."
msgstr ""

#: workflow_actions.py:42
msgid "Roles"
msgstr "角色"

#: workflow_actions.py:44
msgid "Roles whose access will be modified."
msgstr ""

#: workflow_actions.py:51
msgid ""
"Permissions to grant/revoke to/from the role for the object selected above."
msgstr ""

#: workflow_actions.py:59
msgid "Grant access"
msgstr "授权访问"

#: workflow_actions.py:129
msgid "Revoke access"
msgstr "撤销访问"
