# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# jmcainzos <jmcainzos@vodafone.es>, 2014
# jmcainzos <jmcainzos@vodafone.es>, 2015
# Lory977 <helga.carrero@gmail.com>, 2015
# Roberto Rosario, 2015-2018
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:45-0400\n"
"PO-Revision-Date: 2018-09-27 02:31+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Spanish (http://www.transifex.com/rosarior/mayan-edms/language/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:40 links.py:56 models.py:64 queues.py:8 settings.py:10 views.py:627
msgid "Sources"
msgstr "Fuentes"

#: apps.py:55
msgid "Create a document source"
msgstr "Crear una nueva fuente de documentos"

#: apps.py:57
msgid ""
"Document sources are the way in which new documents are feed to Mayan EDMS, "
"create at least a web form source to be able to upload documents from a "
"browser."
msgstr "Las fuentes de documentos son la manera en la que se almacenan nuevos documentos en Mayan EDMS. Crea por lo menos una fuente del tipo formulario web para poder cargar documentos desde un navegador."

#: apps.py:67
msgid "Created"
msgstr "Creado"

#: apps.py:74
msgid "Thumbnail"
msgstr "Foto miniatura"

#: apps.py:82 models.py:789
msgid "Date time"
msgstr "Fecha y hora"

#: apps.py:87 models.py:792
msgid "Message"
msgstr "Mensaje"

#: forms.py:30
msgid "Comment"
msgstr "Comentario"

#: forms.py:45
msgid "Expand compressed files"
msgstr "Expandir archivos comprimidos"

#: forms.py:47
msgid "Upload a compressed file's contained files as individual documents"
msgstr "Subir los archivos de un archivo comprimido como documentos individuales"

#: forms.py:68 views.py:483
msgid "Staging file"
msgstr "Archivo provisional"

#: forms.py:72 forms.py:77
msgid "File"
msgstr "Archivo"

#: handlers.py:16
msgid "Default"
msgstr "Por defecto"

#: links.py:51
msgid "New document"
msgstr "Nuevo documento"

#: links.py:62
msgid "Add new IMAP email"
msgstr "Añadir nuevo correo electrónico IMAP"

#: links.py:67
msgid "Add new POP3 email"
msgstr "Añadir nuevo correo electrónico POP3"

#: links.py:72
msgid "Add new staging folder"
msgstr "Añadir nueva carpeta de ensayo"

#: links.py:77
msgid "Add new watch folder"
msgstr "Añadir nueva carpeta observada"

#: links.py:82
msgid "Add new webform source"
msgstr "Añadir nueva fuente en formato web"

#: links.py:87
msgid "Add new SANE scanner"
msgstr "Añadir un nuevo escáner SANE"

#: links.py:92 links.py:106
msgid "Delete"
msgstr "Borrar"

#: links.py:96
msgid "Edit"
msgstr "Editar"

#: links.py:100
msgid "Document sources"
msgstr "Fuentes de documentos"

#: links.py:111
msgid "Upload new version"
msgstr "Subir versión nueva"

#: links.py:115
msgid "Logs"
msgstr "Bitácoras"

#: links.py:120
msgid "Check now"
msgstr "Revisar ahora"

#: literals.py:16
msgid "Flatbed"
msgstr "Cama plana"

#: literals.py:17
msgid "Document feeder"
msgstr "Alimentador de documentos"

#: literals.py:24
msgid "Simplex"
msgstr "Simple"

#: literals.py:25
msgid "Duplex"
msgstr "Dúplex"

#: literals.py:33
msgid "Lineart"
msgstr "Arte lineal"

#: literals.py:34
msgid "Monochrome"
msgstr "Monocromático"

#: literals.py:35
msgid "Color"
msgstr "Color"

#: literals.py:43 literals.py:48
msgid "Always"
msgstr "Siempre"

#: literals.py:44 literals.py:49
msgid "Never"
msgstr "Nunca"

#: literals.py:50
msgid "Ask user"
msgstr "Preguntar al usuario"

#: literals.py:61
msgid "Scanner"
msgstr "Escáner"

#: literals.py:62 models.py:393
msgid "Web form"
msgstr "Formulario web"

#: literals.py:63 models.py:329
msgid "Staging folder"
msgstr "Archivos provisionales"

#: literals.py:64 models.py:765
msgid "Watch folder"
msgstr "Carpeta observada"

#: literals.py:65
msgid "POP3 email"
msgstr "Correo electrónico POP3"

#: literals.py:66 models.py:662 models.py:663
msgid "IMAP email"
msgstr "Correo electrónico IMAP"

#: models.py:55
msgid "Label"
msgstr "Etiqueta"

#: models.py:57 views.py:592
msgid "Enabled"
msgstr "Habilitado"

#: models.py:63 models.py:786
msgid "Source"
msgstr "Fuente"

#: models.py:176
msgid "Interactive source"
msgstr "Fuente interactiva"

#: models.py:177
msgid "Interactive sources"
msgstr "Fuentes interactivas"

#: models.py:187
msgid "Device name as returned by the SANE backend."
msgstr "Nombre del dispositivo devuelto por el servidor SANE."

#: models.py:188
msgid "Device name"
msgstr "Nombre del dispositivo"

#: models.py:193
msgid ""
"Selects the scan mode (e.g., lineart, monochrome, or color). If this option "
"is not supported by your scanner, leave it blank."
msgstr "Selecciona el modo de escáner (por ejemplo, lineart, monochrome o color). Si esta opción no es compatible con el escáner, déjela en blanco."

#: models.py:195
msgid "Mode"
msgstr "Modo"

#: models.py:199
msgid ""
"Sets the resolution of the scanned image in DPI (dots per inch). Typical "
"value is 200. If this option is not supported by your scanner, leave it "
"blank."
msgstr "Define la resolución de la imagen escaneada en DPI (puntos por pulgada). El valor típico es 200. Si esta opción no es compatible con el escáner, déjelo en blanco."

#: models.py:202
msgid "Resolution"
msgstr "Resolución"

#: models.py:206
msgid ""
"Selects the scan source (such as a document-feeder). If this option is not "
"supported by your scanner, leave it blank."
msgstr "Selecciona la fuente de escaneado (como un alimentador de documentos). Si esta opción no es compatible con el escáner, déjela en blanco."

#: models.py:208
msgid "Paper source"
msgstr "Fuente de papel"

#: models.py:213
msgid ""
"Selects the document feeder mode (simplex/duplex). If this option is not "
"supported by your scanner, leave it blank."
msgstr "Selecciona el modo de alimentador de documentos (simplex / dúplex). Si esta opción no es compatible con el escáner, déjela en blanco."

#: models.py:215
msgid "ADF mode"
msgstr "Modo ADF"

#: models.py:221
msgid "SANE Scanner"
msgstr "Escáner SANE"

#: models.py:222
msgid "SANE Scanners"
msgstr "Escáneres SANE"

#: models.py:269
#, python-format
msgid "Error while scanning; %s"
msgstr "Error al escanear; %s"

#: models.py:301 models.py:758
msgid "Server side filesystem path."
msgstr "Ruta a los archivos en el servidor."

#: models.py:302 models.py:759
msgid "Folder path"
msgstr "Ruta de la carpeta"

#: models.py:305
msgid "Width value to be passed to the converter backend."
msgstr "Valor de la anchura que se pasa al backend convertidor."

#: models.py:306
msgid "Preview width"
msgstr "Ancho de muestra"

#: models.py:310
msgid "Height value to be passed to the converter backend."
msgstr "Valor de la altura que se pasa al backend convertidor."

#: models.py:311
msgid "Preview height"
msgstr "Alto de muestra"

#: models.py:315 models.py:386
msgid "Whether to expand or not compressed archives."
msgstr "Expandir o no archivos comprimidos."

#: models.py:316 models.py:387 models.py:427
msgid "Uncompress"
msgstr "Descomprimir"

#: models.py:321
msgid "Delete the file after is has been successfully uploaded."
msgstr "Eliminar el archivo después de que se haya cargado correctamente."

#: models.py:323
msgid "Delete after upload"
msgstr "Borrar después de subir"

#: models.py:330
msgid "Staging folders"
msgstr "Archivos provisionales"

#: models.py:342
#, python-format
msgid "Error deleting staging file; %s"
msgstr "Error al borrar archivo de ensayo; %s"

#: models.py:358
#, python-format
msgid "Unable get list of staging files: %s"
msgstr "No es posible obtener la lista de los archivos provisionales: %s"

#: models.py:394
msgid "Web forms"
msgstr "Formularios web"

#: models.py:407 models.py:408
msgid "Out of process"
msgstr "Fuera de proceso"

#: models.py:414
msgid "Interval in seconds between checks for new documents."
msgstr "Intérvalo en segundos para detectar documentos nuevos"

#: models.py:415
msgid "Interval"
msgstr "Intérvalo"

#: models.py:420
msgid "Assign a document type to documents uploaded from this source."
msgstr "Asignar un tipo de documento a los documentos subidos desde esta fuente"

#: models.py:422
msgid "Document type"
msgstr "Tipo de documento"

#: models.py:426
msgid "Whether to expand or not, compressed archives."
msgstr "Expandir o no archivos comprimidos."

#: models.py:433
msgid "Interval source"
msgstr "Intervalo de fuente."

#: models.py:434
msgid "Interval sources"
msgstr "Intervalo de fuentes"

#: models.py:492
msgid "Host"
msgstr "Host"

#: models.py:493
msgid "SSL"
msgstr "SSL"

#: models.py:495
msgid ""
"Typical choices are 110 for POP3, 995 for POP3 over SSL, 143 for IMAP, 993 "
"for IMAP over SSL."
msgstr "Las opciones típicas son 110 para POP3, 995 para POP3 sobre SSL, 143 para IMAP, 993 para IMAP sobre SSL."

#: models.py:496
msgid "Port"
msgstr "Puerto"

#: models.py:498
msgid "Username"
msgstr "Usuario"

#: models.py:499
msgid "Password"
msgstr "Contraseña"

#: models.py:503
msgid ""
"Name of the attachment that will contains the metadata type names and value "
"pairs to be assigned to the rest of the downloaded attachments. Note: This "
"attachment has to be the first attachment."
msgstr "Nombre del archivo adjunto que contiene los nombres de los tipos de metadatos y los pares de valores que se asignará al resto de los archivos adjuntos descargados. Nota: Este anejo tiene que ser el primer archivo adjunto."

#: models.py:507
msgid "Metadata attachment name"
msgstr "Nombre del anejo de metadatos"

#: models.py:511
msgid ""
"Select a metadata type valid for the document type selected in which to "
"store the email's subject."
msgstr "Seleccione un tipo de metadatos válido para el tipo de documento seleccionado para almacenar el asunto del correo electrónico."

#: models.py:514
msgid "Subject metadata type"
msgstr "Tipo de metadatos de asunto "

#: models.py:518
msgid ""
"Select a metadata type valid for the document type selected in which to "
"store the email's \"from\" value."
msgstr "Seleccione un tipo de metadatos válido para el tipo de documento seleccionado para almacenar el valor \"de\" del correo electrónico."

#: models.py:521
msgid "From metadata type"
msgstr "Tipo de metadato de remitente"

#: models.py:525
msgid "Store the body of the email as a text document."
msgstr "Almacenar el cuerpo del correo electrónico como un documento de texto."

#: models.py:526
msgid "Store email body"
msgstr "Almacenar cuerpo del correo electrónico"

#: models.py:532
msgid "Email source"
msgstr "Fuente de correo electrónico"

#: models.py:533
msgid "Email sources"
msgstr "Fuentes de correo electrónico"

#: models.py:541
#, python-format
msgid ""
"Subject metadata type \"%(metadata_type)s\" is not valid for the document "
"type: %(document_type)s"
msgstr "El tipo de metadatos de tema \"%(metadata_type)s\" no es válido para el tipo de documento: %(document_type)s"

#: models.py:555
#, python-format
msgid ""
"\"From\" metadata type \"%(metadata_type)s\" is not valid for the document "
"type: %(document_type)s"
msgstr "\"De\" tipo de metadatos \"%(metadata_type)s\" no es válido para el tipo de documento: %(document_type)s"

#: models.py:655
msgid "IMAP Mailbox from which to check for messages."
msgstr "Buzón IMAP en el cual revisar mensajes."

#: models.py:656
msgid "Mailbox"
msgstr "Buzón"

#: models.py:701
msgid "Timeout"
msgstr "Tiempo de espera"

#: models.py:707 models.py:708
msgid "POP email"
msgstr "Correo electrónico POP"

#: models.py:766
msgid "Watch folders"
msgstr "Carpetas observadas"

#: models.py:798
msgid "Log entry"
msgstr "Entrada de bitácora"

#: models.py:799
msgid "Log entries"
msgstr "Entradas de bitácora"

#: permissions.py:7
msgid "Sources setup"
msgstr "Configuración de fuentes de documentos"

#: permissions.py:9
msgid "Create new document sources"
msgstr "Crear nuevas fuentes de documentos"

#: permissions.py:12
msgid "Delete document sources"
msgstr "Eliminar fuentes de documentos"

#: permissions.py:15
msgid "Edit document sources"
msgstr "Editar fuentes de documentos"

#: permissions.py:18
msgid "View existing document sources"
msgstr "Ver fuentes de documento existentes"

#: permissions.py:21
msgid "Delete staging files"
msgstr "Borrar archivos provisionales"

#: queues.py:11
msgid "Sources periodic"
msgstr "Fuentes periódicas"

#: queues.py:14
msgid "Sources fast"
msgstr ""

#: queues.py:19
msgid "Generate staging file image"
msgstr ""

#: queues.py:23
msgid "Check interval source"
msgstr "Intervalo de comprobación de la fuente"

#: queues.py:27
msgid "Handle upload"
msgstr "Manejar la carga"

#: queues.py:31
msgid "Upload document"
msgstr "Subir documento"

#: settings.py:15
msgid "File path to the scanimage program used to control image scanners."
msgstr "Ruta de acceso al programa scanimage utilizado para controlar los escáneres de imágenes."

#: settings.py:22
msgid ""
"Path to the Storage subclass to use when storing the cached staging_file "
"image files."
msgstr ""

#: settings.py:31
msgid "Arguments to pass to the SOURCES_STAGING_FILE_CACHE_STORAGE_BACKEND."
msgstr ""

#: tasks.py:46
#, python-format
msgid "Error processing source: %s"
msgstr "Error procesando fuente: %s"

#: views.py:66
msgid ""
"Any error produced during the usage of a source will be listed here to "
"assist in debugging."
msgstr "Cualquier error producido durante el uso de una fuente se enumerará aquí para ayudar en la depuración."

#: views.py:69
msgid "No log entries available"
msgstr "No hay entradas de registro disponibles"

#: views.py:71
#, python-format
msgid "Log entries for source: %s"
msgstr "Entradas de bitácora para fuente: %s"

#: views.py:127 wizards.py:145
msgid ""
"No interactive document sources have been defined or none have been enabled,"
" create one before proceeding."
msgstr "No se han definido fuentes de documentos interactivos o no hay ninguna habilitada, cree una antes de continuar."

#: views.py:153 views.py:171 views.py:181
msgid "Document properties"
msgstr "Propiedades de documento"

#: views.py:161
msgid "Files in staging path"
msgstr "Archivos en ruta de ensayo"

#: views.py:172
msgid "Scan"
msgstr "Escanear"

#: views.py:281
#, python-format
msgid ""
"Error executing document upload task; %(exception)s, %(exception_class)s"
msgstr "Error al ejecutar la tarea de carga de documentos; %(exception)s, %(exception_class)s"

#: views.py:295
msgid "New document queued for upload and will be available shortly."
msgstr "Nuevo documento puesto en cola para su carga y estará disponible en breve."

#: views.py:346
#, python-format
msgid "Upload a document of type \"%(document_type)s\" from source: %(source)s"
msgstr ""

#: views.py:379
#, python-format
msgid "Document \"%s\" is blocked from uploading new versions."
msgstr "Documento \"%s\" esta bloqueado de crear nuevas versiones."

#: views.py:431
msgid "New document version queued for upload and will be available shortly."
msgstr "Nueva versión del documento puesto en cola para su carga y estará disponible en breve."

#: views.py:470
#, python-format
msgid "Upload a new version from source: %s"
msgstr "Subir una nueva versión de la fuente: %s"

#: views.py:510
#, python-format
msgid "Trigger check for source \"%s\"?"
msgstr "¿Lanzar chequeo para la fuenta \"%s\"? "

#: views.py:523
msgid "Source check queued."
msgstr "Verificación del origen en sometida."

#: views.py:537
#, python-format
msgid "Create new source of type: %s"
msgstr "Crear nuevo tipo de fuente: %s"

#: views.py:557
#, python-format
msgid "Delete the source: %s?"
msgstr "¿Eliminar la fuente: %s?"

#: views.py:576
#, python-format
msgid "Edit source: %s"
msgstr "Editar fuente: %s"

#: views.py:588
msgid "Type"
msgstr "Tipo"

#: views.py:621
msgid ""
"Sources provide the means to upload documents. Some sources like the "
"webform, are interactive and require user input to operate. Others like the "
"email sources, are automatic and run on the background without user "
"intervention."
msgstr "Las fuentes proporcionan los medios para cargar documentos. Algunas fuentes, como el formulario web, son interactivas y requieren la intervención del usuario para operar. Otros, como las fuentes de correo electrónico, son automáticos y se ejecutan en segundo plano sin la intervención del usuario."

#: views.py:626
msgid "No sources available"
msgstr "No hay fuentes disponibles"

#: wizards.py:96
msgid "Select document type"
msgstr "Seleccione el tipo de documento"

#: wizards.py:164
#, python-format
msgid "Step %(step)d of %(total_steps)d: %(step_label)s"
msgstr "Paso %(step)d de %(total_steps)d: %(step_label)s"

#: wizards.py:169
msgid "Next step"
msgstr "Siguiente paso"

#: wizards.py:171
msgid "Document upload wizard"
msgstr "Asistente de carga de documentos"
