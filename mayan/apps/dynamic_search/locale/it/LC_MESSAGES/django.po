# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Carlo Zanatto <>, 2012
# Giovanni Tricarico <gtricarico92@gmail.com>, 2014
# Marco Camplese <marco.camplese.mc@gmail.com>, 2016-2017
# Pierpaolo Baldan <pierpaolo.baldan@gmail.com>, 2011
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:43-0400\n"
"PO-Revision-Date: 2018-09-27 02:30+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Italian (http://www.transifex.com/rosarior/mayan-edms/language/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:16
msgid "Dynamic search"
msgstr "Ricerca dinamica"

#: classes.py:40
msgid "No search model matching the query"
msgstr "Nessun modello di ricerca corrisponde alla query"

#: forms.py:9
msgid "Match all"
msgstr "Corrisponde a tutti"

#: forms.py:10
msgid ""
"When checked, only results that match all fields will be returned. When "
"unchecked results that match at least one field will be returned."
msgstr ""

#: forms.py:29
msgid "Search terms"
msgstr "Cerca termini "

#: links.py:8 settings.py:8 views.py:58 views.py:69
msgid "Search"
msgstr "Cerca"

#: links.py:11 views.py:83
msgid "Advanced search"
msgstr "Ricerca avanzata"

#: links.py:15
msgid "Search again"
msgstr "Cerca ancora"

#: settings.py:11
msgid "Maximum amount search hits to fetch and display."
msgstr "Massimo numero ricerca risultati da recuperare e visualizzare. "

#: views.py:26
msgid "Try again using different terms. "
msgstr ""

#: views.py:28
msgid "No search results"
msgstr ""

#: views.py:31
#, python-format
msgid "Search results for: %s"
msgstr "Risultati ricerca per: %s"

#: views.py:71
#, python-format
msgid "Search for: %s"
msgstr "Cerca: %s"
